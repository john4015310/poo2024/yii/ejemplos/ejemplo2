<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Gestion de Pedidos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gestion de Almacen</h1>

        <p class="lead">Gestion de Comerciales, Clientes y Pedido.</p>

        <p><a class="btn btn-lg btn-success" href="https://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 mb-3 ">
                <h2>Clientes</h2>
                <p><?= Html::a('Ir a Clientes', ['cliente/index'], ['class' => 'btn btn-outline-secondary']) ?></p>
            </div>
            <div class="col-lg-4 mb-3">
                <h2>Comerciales</h2>
                <p><?= Html::a('Ir a Comerciales', ['comercial/index'], ['class' => 'btn btn-outline-secondary']) ?></p>
            </div>
            <div class="col-lg-4 mb-3">
                <h2>Pedido</h2>
                <p><?= Html::a('Ir a Pedido', ['pedido/index'], ['class' => 'btn btn-outline-secondary']) ?></p>
            </div>
        </div>

    </div>
</div>
