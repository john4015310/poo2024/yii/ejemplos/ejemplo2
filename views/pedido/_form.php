<?php

use app\models\Cliente;
use app\models\Comercial;
use app\models\Pedido;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?php 
        // //use app\models\Country;
        // $clientes=Cliente::find()->all();

        // //use yii\helpers\ArrayHelper;
        // $listData=ArrayHelper::map($clientes,'id','nombre');

        //Hemos cambiado la recogida de los cliente con un metodo statico 
        //------LO PODEMOS PONER CON IMPRESION CORTA ------------------
        echo $form->field($model, 'id_cliente')->dropDownList(
                Pedido::todosClientes(), //aqui cambip el statico por el listdata que recibia antes
                ['prompt'=>'Selecciona un cliente']
                );
    ?>
    <?php 
        //use app\models\Country;
            // $comerciales=Comercial::find()->all();

        //use yii\helpers\ArrayHelper;
        // $listData=ArrayHelper::map($comerciales,'id','nombre');

        //-------------------------------------------------------------
        //remplazo lo de arriba ya que tengo un array de objetos
        // en una action

        echo $form->field($model, 'id_comercial')->dropDownList(
                $model->todosComerciales(),
                ['prompt'=>'Selecciona un comercial']
                );
    ?>

    <!-- <?= $form->field($model, 'id_comercial')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
